#include "RE_qubit.h"
#include "Helper_functions.h"
#include <cmath>
#include <iostream>
#include <complex>
#include "mex.h"



static const std::complex<double> i(0.0, 1.0);

RE_qubit::RE_qubit(double* _energy_levels, double* _basis_levels,
                     double* _transition_lw,
                     double* _osc_strengths,
                     double* _T1,
                     int _num_states)
    : energy_levels(_energy_levels)
    ,basis_levels(_basis_levels)
    , transition_lw(_transition_lw)
    , osc_strengths(_osc_strengths)
    , T1(_T1)
    , num_states(_num_states)
{
    H.resize(num_states * num_states);

    decay_prob.resize(num_states*num_states);
    double sum;
    for (int ii=num_states-1;ii!= -1;--ii)
    {
        sum =0;
        for(int jj=0;jj!=ii;++jj)
        {
            sum += _osc_strengths[ii*num_states+jj]*exp(-T1[ii*num_states+jj]);
        }
        for(int jj=0;jj!=ii;++jj)
        {
            if (sum !=0)
            {
                decay_prob[ii*num_states+jj]=_osc_strengths[ii*num_states+jj]*exp(-T1[ii*num_states+jj])/sum;
                decay_prob[jj*num_states+ii]=_osc_strengths[ii*num_states+jj]*exp(-T1[ii*num_states+jj])/sum;
            }
            else //decay time is bloody ages, just assume all T1 times below this are the same
            {
                decay_prob[ii*num_states+jj]=_osc_strengths[ii*num_states+jj]/ii;
                decay_prob[jj*num_states+ii]=_osc_strengths[ii*num_states+jj]/ii;
            }
        }

    }

}

void RE_qubit::find_resonant(double curr_freq,double dt,int field_num)
{

if (field_num >= res_trans.size())
{
    int old_size;
    res_trans.resize(field_num+1);
    res_freqs.resize(field_num+1);
}
        this->res_trans[field_num].clear();
        this->res_freqs[field_num]=curr_freq;

        for(int ii = 0; ii != num_states; ++ii)
        {
            for(int jj = ii + 1; jj != num_states; ++jj)
            {
            //std::cout <<  osc_strengths[ii*num_states+jj]/(std::fabs(curr_freq+energy_levels[ii]-energy_levels[jj]))<<std::endl;
                //if (dt < 5*osc_strengths[ii*num_states+jj]/(std::fabs(curr_freq+energy_levels[ii]-energy_levels[jj])))
                if (osc_strengths[ii*num_states+jj] > 0)
                    this->res_trans[field_num].push_back(std::pair<int,int>(ii,jj));
            }
        }
}


int RE_qubit::get_num_states()
{
    return this->num_states;
}




void RE_qubit::Hamiltonian(const double time,const double dt,std::vector<double> curr_freq,std::vector<double> curr_field,state_iter H_iter)
{


    //clear H_iter
    for(int ii = 0; ii != num_states*num_states; ++ii) {
                H_iter[ii] = 0;
    }
    for (int ii=0;ii!= num_states;++ii) //diagonal terms (nonzero for inhomogeneously broadened atoms)
    {
        H_iter[num_states*ii+ii] = energy_levels[ii] - basis_levels[ii];
    }



     for (int ff=0;ff!=curr_field.size();++ff)
     {
        if (ff>=res_freqs.size() || curr_freq[ff] != res_freqs[ff]) //if the frequency has changed find the new resonant transitions
        {

             this->find_resonant(curr_freq[ff],dt,ff);
        }
        for (int ii=0;ii!= this->res_trans[ff].size();++ii) //off diagonal terms
        {
            int s1 = this->res_trans[ff][ii].first;
            int s2 = this->res_trans[ff][ii].second;

             H_iter[num_states * (s1)+s2] += 0.5*curr_field[ff]*this->osc_strengths[num_states * (s1)+s2]*exp(i*(curr_freq[ff]+basis_levels[s1]-basis_levels[s2]) * time);
             H_iter[num_states * (s2)+s1] += 0.5*curr_field[ff]*this->osc_strengths[num_states * (s1)+s2]*exp(-i*(curr_freq[ff]+basis_levels[s1]-basis_levels[s2]) * time);

        }
    }
}


//Returns the part of the Hamiltonian proportional to a given driving field
void RE_qubit::Optical_Hamiltonian(const double time,const double dt,double curr_freq,int ff,state_iter H_iter)
{

    //clear H_iter
    for(int ii = 0; ii != num_states*num_states; ++ii) {
                H_iter[ii] = 0;
    }


    if (ff>=res_freqs.size() || curr_freq != res_freqs[ff])
    {
            this->find_resonant(curr_freq,dt,ff);
    }
        for (int ii=0;ii!= this->res_trans[ff].size();++ii)
        {

            int s1 = this->res_trans[ff][ii].first;
            int s2 = this->res_trans[ff][ii].second;
            H_iter[num_states * (s1)+s2] += 0.5*this->osc_strengths[num_states * (s1)+s2]*exp(i*(curr_freq+basis_levels[s1]-basis_levels[s2]) * time);
            H_iter[num_states * (s2)+s1] += 0.5*this->osc_strengths[num_states * (s1)+s2]*exp(-i*(curr_freq+basis_levels[s1]-basis_levels[s2]) * time);

        }

}



