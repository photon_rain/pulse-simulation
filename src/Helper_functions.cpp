#include "Helper_functions.h"
#include <algorithm>
#include <complex>
#include <iostream>
#include <cmath>
#include <vector>
#include "mex.h"

typedef std::vector<std::complex<double> >::iterator state_iter;
typedef std::vector<std::complex<double> >::const_iterator state_iter_const;
typedef std::vector<std::complex<double> > state_type;

double interpolate(double start_x, double end_x, double start_y, double end_y, double x)
{
    return start_y + (x - start_x) / (end_x - start_x) * (end_y - start_y);
}


// commutator of two Hermitian matrices (resulting matrix is anti-hermitian)
void Herm_comm(const state_iter_const m1_s, const state_iter_const m1_e, const state_iter_const m2_s, const state_iter_const m2_e, state_iter output)
{
    int num_states = std::sqrt(m1_e - m1_s);
    std::complex<double> matrix_elem;

    //looping over rows and columns of m1 instead of the resultant (this lets us skip lots of values)
    for(int ii = 0; ii != num_states; ++ii) { // rows
        for(int jj = 0; jj != num_states; ++jj) { // columns
            if ((m1_s[num_states * ii + jj].real()  != 0.0 || m1_s[num_states * ii + jj].imag())  != 0.0 || (m2_s[num_states * ii + jj].real()  != 0.0 || m2_s[num_states * ii + jj].imag()!=0.0))
            {
                for(int kk = ii; kk != num_states; ++kk)
                {
                    if (m2_s[num_states*jj+kk].real() != 0.0 || m2_s[num_states*jj+kk].imag()!=0.0 || m1_s[num_states*jj+kk].real() != 0.0 || m1_s[num_states*jj+kk].imag()!=0.0  )
                    {
                        matrix_elem = (m1_s[num_states * ii + jj] * m2_s[num_states * jj + kk]) - m2_s[num_states * ii + jj] * m1_s[num_states * jj +kk];
                        output[num_states * ii + kk] += matrix_elem;
                        if (kk !=ii)
                            output[num_states * kk + ii] += -std::conj(matrix_elem);
                    }
                }

            }
        }
    }
}





state_type outer_prod(const state_type& m1, const state_type& m2,int dim1R,int dim2R)
{

    std::complex<double> matrix_elem;

    int dim1C = (m1.size()) / dim1R;
    int dim2C = (m2.size()) / dim2R;
    state_type output(dim1R * dim2R * dim1C * dim2C);


    for(int row1 = 0; row1 != dim1R; ++row1) { // rows

        for(int col1 = 0; col1 != dim1C; ++col1) { // columns
            for(int row2 = 0; row2 != dim2R; ++row2) {
                for(int col2 = 0; col2 != dim2C; ++col2) {
                    output[dim1R * dim2R * (row2 + row1 * dim2R) + (col2 + col1 * dim2C)] = m1[dim1R * row1 + col1] * m2[dim2R * row2 + col2];
                }
            }
        }
    }
    return output;
}

//Take the outer product M x I
state_type outer_prod_I(const state_type& m1,int dim1,int dimI)
{
    state_type output(dim1 * dim1 * dimI * dimI,0);
    int m1R;
    int IR;
    int m1C;
    int IC;

    for(int row = 0; row != dim1*dimI; ++row)
    {
        for(int col = 0; col != dim1*dimI; ++col)
        {
            m1R = row % dim1;
            IR = row / dimI;
            m1C = col % dim1;
            IC = col / dimI;
            if (IR== IC) //else zero
                output[row*dim1*dimI+col]= m1[m1R*dim1+m1C];
        }
    }
    return output;
}


//Takes the outer product I^k x M x I^l and adds it to a matrix
//assumes that m1 is hermitian
state_type add_expanded_basis(const state_type& m1,int dim1,int dimI, int k,int l,state_type& output)
{

    int out_R;
    int out_C;

    int dim_k = (int)std::pow(dimI,k);
    int dim_l = (int)std::pow(dimI,l);
    int dim_out = dim1*dim_k*dim_l;

    for (int row =0;row!=dim1;++row)
    {
        for (int col=0;col != dim1;++col)
        {
            for (int k_idx=0;k_idx!=dim_k;++k_idx)
            {
            for (int l_idx =0;l_idx!=dim_l;++l_idx)
            {
            //utilising the fact that (I_n)^k outer product n times is I_(n^k)
                    if (k==0 &&l==0)
                    {
                        output = m1;
                        return output;
                    }
                    else if (k==0) //then we are finding M x I
                    {
                        out_R= row*dim_l+l_idx;
                        out_C= col*dim_l+l_idx;
                    }
                    else if (l==0)//then we are finding I x M
                    {
                        out_R = row+k_idx*dim1;
                        out_C = col+k_idx*dim1;
                    }
                    else
                    {
//                        out_R = row + k_idx*(2*dim_l)+l_idx;
//                        out_C =
                    }
                    output[out_R*dim_out+out_C]+= m1[row*dim1+col];
            }
            }
        }
    }
    return output;
}



//multiply two matrices
state_type mult_m(const state_type& m1,const state_type& m2,int row1,int row2)
{
    int col1 = (m1.size())/row1;
    int col2 = (m2.size())/row2;

    state_type results(row1*col2);


    for (int rr =0;rr!= row1;++rr) //m1 row
    {
        for (int cc=0; cc!= col2;++cc) //m1 column
        {
            if (m1[row1 * rr + cc].real() != 0.0 || m1[row1 * rr + cc].imag() !=0.0)
            {
                for (int ii=0;ii!= col1;++ii) //output column
                {
                        results[rr*row1+cc] += m1[row1 * rr + cc] * m2[row2 * cc + ii];
                }
            }
        }
    }
    return results;
}




//assumes Hermitian
state_type conj_transpose(const state_type& m_orig,int row_length)
{
    state_type m_dagger(m_orig.size(),0);
    int col_length = m_orig.size()/row_length;
    for (int ii = 0; ii != row_length;++ii)
    {
        for (int jj =0;jj != col_length;++jj)
        {
            if (m_orig[ii*row_length + jj].real() != 0.0 && m_orig[ii*row_length + jj].imag() != 0.0 )
            {
                m_dagger[jj*row_length+ii] = std::conj(m_orig[ii*row_length + jj]);
            }
        }
    }
    return m_dagger;
}




//adds two matrices elementwise
void add_m(state_iter v1_b,state_iter v1_e, state_iter_const v2_b)
{
    std::transform(v1_b, v1_e, v2_b, v1_b, std::plus<std::complex<double>>());
}

//adds two matrices elementwise
void add_m(double* v1_b,double* v1_e,double* v2_b)
{
    std::transform(v1_b, v1_e, v2_b, v1_b, std::plus<double>());
}


//multiply a matrix by a constant factor
void const_mult_m(state_type& vec_orig, std::complex<double> factor)
{
    std::transform(vec_orig.begin(), vec_orig.end(), vec_orig.begin(), std::bind1st(std::multiplies<std::complex<double> >(),factor));
}





//for calculating mixed state fidelity
double fidelity(state_iter_const rho1,state_iter_const rho2,mwSize matrix_dim)
{
    mxArray* mexArray1;
    mxArray* mexArray2;

    mxArray *mex_fid;



    mwSize dims[2] = {matrix_dim,matrix_dim};
    mexArray1 = mxCreateNumericArray(2, dims,mxDOUBLE_CLASS,  mxCOMPLEX);
    mexArray2 = mxCreateNumericArray(2, dims,mxDOUBLE_CLASS,  mxCOMPLEX);

    mxArray *rhs[2]= {mexArray1,mexArray2};


    double *arr1_real = mxGetPr(mexArray1);
    double *arr1_imag = mxGetPi(mexArray1);

    double *arr2_real = mxGetPr(mexArray2);
    double *arr2_imag = mxGetPi(mexArray2);

    for (int ii = 0; ii != matrix_dim*matrix_dim; ++ii )
    {
        arr1_real[ii]= std::real(rho1[ii]);
        arr1_imag[ii]= std::imag(rho1[ii]);
        arr2_real[ii]= std::real(rho2[ii]);
        arr2_imag[ii]= std::imag(rho2[ii]);
    }
    mexCallMATLAB(1, &mex_fid, 2, rhs, "fidelity");
    return *mxGetPr(mex_fid);

}

//calculates the fidelity between two pure states
double Hermitian_fidelity(state_type& rho1,state_type& rho2,int matrix_dim)
{

return trace(mult_m(rho1,rho2,matrix_dim,matrix_dim));
}




double trace(const state_type& matrix)
{
    int m_dim = std::sqrt(matrix.size());
    double sum =0;
    for (int ii=0;ii!= m_dim;++ii)
    {
        sum+= matrix[ii*m_dim+ii].real();
    }
    return sum;
}


