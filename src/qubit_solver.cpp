#include "mex.h"
#include "RE_qubit.h"
#include "N_qubit_ODE.h"
#include "boost/numeric/odeint/stepper/runge_kutta4.hpp"
#include "Helper_functions.h"
#include <complex>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <cmath>

#include <thread>
#include <mutex>

using namespace boost::numeric::odeint;

#define num_laser_params 2
#define num_ion_params 4
#define num_qubits 1
std::mutex m;

void evolve_ions(std::vector<N_qubit_ODE> qubit_systems,int start_ions,int end_ions,state_type rho_I,double * pulse_time,int pulse_length,double* output_real_sys,double* output_imag_sys,double* output_time)
{
    runge_kutta4<std::vector<std::complex<double> > > stepper;
    state_type rho = rho_I;
    int num_ions = qubit_systems.size();
    for (int ii=start_ions;ii!=end_ions;++ii )
    {
        rho=rho_I;

        for(int jj = 1; jj != pulse_length; ++jj) {
            output_time[jj] = pulse_time[jj];
            stepper.do_step(qubit_systems[ii], rho, pulse_time[jj], pulse_time[jj] - pulse_time[jj - 1]);

            m.lock();
            for(int kk = 0; kk != rho.size(); ++kk) {
                output_real_sys[jj+ kk * pulse_length] += rho[kk].real();
                output_imag_sys[jj+ kk * pulse_length] += rho[kk].imag();
            }
            m.unlock();

        }

    }
}





void evolve_system(std::vector<N_qubit_ODE> qubit_systems,state_type rho_I,double * pulse_time,int pulse_length,double* output_real_sys,double* output_imag_sys,double* output_time)
{
    //number of concurrent threads
    unsigned max_conc_threads = std::thread::hardware_concurrency();
    if (max_conc_threads ==0) //returns 0 on failure so need to check for this
        max_conc_threads=1;

    int num_ions = qubit_systems.size();

    int ions_per_qubit = num_ions / max_conc_threads;

    std::vector<std::thread> thread_vec(max_conc_threads);

    if (num_ions < max_conc_threads)
        max_conc_threads = num_ions;

    int rho_size =0;
    if (num_qubits ==1)
        rho_size = std::pow(qubit_systems[0].get_num_states(0),2);
    else
        rho_size = std::pow(qubit_systems[0].get_num_states(0)*qubit_systems[0].get_num_states(1),2);
    //initialise our output matrix
    for(int kk = 0; kk != rho_size; ++kk) {
        output_real_sys[kk * (pulse_length)] = num_ions*rho_I[kk].real();
        output_imag_sys[kk * (pulse_length)] = num_ions*rho_I[kk].imag();
    }


    for (int ii=0;ii!= max_conc_threads;++ii)
    {
        int start_ions= ions_per_qubit*ii;
        int end_ions = ions_per_qubit*(ii+1);
        if (ii==max_conc_threads-1)
        {
            end_ions = num_ions;
        }
    // //     evolve_ions(qubit_systems,start_ions,end_ions,rho_I,pulse_time,pulse_length,output_real_sys,output_imag_sys,output_time);
        thread_vec[ii]=std::thread(evolve_ions,qubit_systems,start_ions,end_ions,rho_I,pulse_time,pulse_length,output_real_sys,output_imag_sys,output_time);
    }

    for (int ii=0;ii!=max_conc_threads;++ii)
    {
        thread_vec[ii].join();
    }


    for (int ii=0;ii!=rho_size*pulse_length;++ii)
    {
        output_imag_sys[ii]=output_imag_sys[ii]/num_ions;
        output_real_sys[ii]=output_real_sys[ii]/num_ions;
    }

}



void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
    //---------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------Variable Initialisation------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------
    double* pulse_time;
    mwSize pulse_length;
    double* pulse_vec;
    double* laser_freq_vec;
    double laser_lw;

    // properties that are the same for both ions
    mwSize rho_size;

    // Properties unique to first qubit
    double* energy_levelsA;
    double* transition_lw;
    double* osc_strengths;
    double* int_matrix;
    double* T1;
    double* rho_real;
    double* rho_imag;


    double* energy_levelsB;


    double* output_real_sys;
    double* output_imag_sys;
    double* output_time;


    RE_qubit ode_sysA;
    RE_qubit ode_sysB;
    std::vector<RE_qubit> qubits;
    std::vector<N_qubit_ODE> qubit_systems;



    // Make sure the arguments are correct
    if(nrhs != 6) {
        mexErrMsgTxt("Wrong number of input arguments.");
    } else if(nlhs > 2) {
        mexErrMsgTxt("Too many output arguments.");
    }

    // make sure the parameters are correct
    if(mxGetNumberOfFields(prhs[2]) != num_laser_params) {
        mexErrMsgTxt("Wrong number of laser parameters.");
    }
    if(mxGetNumberOfFields(prhs[5]) != num_ion_params) {
        mexErrMsgTxt("Wrong number of ion properties.");
    }

    pulse_length = mxGetN(prhs[0]);
    pulse_time = mxGetPr(prhs[0]);

    rho_real = mxGetPr(prhs[1]);
    rho_imag = mxGetPi(prhs[1]);

    rho_size = std::pow(mxGetN(prhs[1]),2);
    int num_pulses = mxGetM(mxGetFieldByNumber(prhs[2], 0, 0));

    // Get pulse parameters
    pulse_vec=mxGetPr(mxGetFieldByNumber(prhs[2], 0, 0));
    laser_freq_vec=mxGetPr(mxGetFieldByNumber(prhs[2], 0, 1));


    int num_ions = mxGetN(prhs[3]);

    plhs[0] = mxCreateDoubleMatrix(pulse_length, 1, mxREAL);


    mwSize dims[3] = {pulse_length,rho_size};
    plhs[1] = mxCreateNumericArray(2, dims,mxDOUBLE_CLASS,  mxCOMPLEX);
    output_real_sys = mxGetPr(plhs[1]);
    output_imag_sys = mxGetPi(plhs[1]);

    output_time = mxGetPr(plhs[0]);
    output_time[0] = pulse_time[0];

    int num_states1 = mxGetM(prhs[3]);
    int num_states2 = mxGetM(prhs[4]);
    state_type rho(rho_size);

    energy_levelsA = mxGetPr(prhs[3]);
    energy_levelsB = mxGetPr(prhs[4]);

    //Get shared parameters
    transition_lw = mxGetPr(mxGetFieldByNumber(prhs[5], 0, 0));
    osc_strengths = mxGetPr(mxGetFieldByNumber(prhs[5], 0, 1));
    int_matrix = mxGetPr(mxGetFieldByNumber(prhs[5], 0, 2));
    T1 = mxGetPr(mxGetFieldByNumber(prhs[5], 0, 3));


    //so we can model all our ions in the same basis (useful for echos and FID)
    double basis_levelsA[num_states1]={0.0};
    double basis_levelsB[num_states2]={0.0};
    for (int ii=0;ii!= num_ions;++ii)
    {
        for (int jj=0;jj!=num_states1;++jj)
        {
            basis_levelsA[jj] += energy_levelsA[ii*num_states1+jj]/num_ions;
        }
        for (int jj=0;jj!=num_states2;++jj)
        {
            basis_levelsB[jj] += energy_levelsB[ii*num_states2+jj]/num_ions;
        }
    }

    //-------------------------------------------------------------------------------
    //-----------------looping over all the ions----------------------------
    //--------------------------------------------------------------------------------
    for (int ii=0; ii!=num_ions; ++ii) {




        if(rho_imag != NULL) {
            for(int ii = 0; ii != rho_size; ++ii)
                rho[ii] = std::complex<double>(rho_real[ii], rho_imag[ii]);
        } else {
            for(int ii = 0; ii != rho_size; ++ii)
                rho[ii] = std::complex<double>(rho_real[ii], 0);
        }
        qubits.clear();
        ode_sysA = RE_qubit(energy_levelsA+ii*num_states1,basis_levelsA,transition_lw,osc_strengths,T1,num_states1);
        qubits.push_back(ode_sysA);

        if (num_qubits !=1)
        {
            ode_sysB = RE_qubit(energy_levelsB+ii*num_states2,basis_levelsB,transition_lw,osc_strengths,T1,num_states2);
            qubits.push_back(ode_sysB);

        }
        
        qubit_systems.push_back(N_qubit_ODE (qubits, state_type(int_matrix,int_matrix+rho_size),pulse_vec,pulse_time,laser_freq_vec,pulse_length,num_pulses));

    }

    evolve_system(qubit_systems, rho,pulse_time,pulse_length,output_real_sys,output_imag_sys,output_time); //do our actual computation
}


