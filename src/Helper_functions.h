#ifndef HELPER_FUNC
#define HELPER_FUNC


#include <algorithm>
#include <complex>
#include <iostream>
#include <cmath>
#include <vector>

typedef std::vector<std::complex<double> >::iterator state_iter;
typedef std::vector<std::complex<double> >::const_iterator state_iter_const;
typedef std::vector<std::complex<double> > state_type;

  template <typename T> T* find_nearest(double* start, double* end, T search_value)
{
    T* lower = std::lower_bound(start,end,search_value);
    T* upper = std::upper_bound(end-1,start-1,search_value);

    T* nearest;

    if (lower ==start || upper ==end)
        nearest = end;
    else
    {
        if (search_value-*lower < *upper-search_value)
        {
             nearest=lower;
        }
        else
        {
            nearest=upper;
        }
    }
    return nearest;
}



double interpolate(double,double, double, double, double);

void Herm_comm(state_iter_const,const state_iter_const ,const state_iter_const,const state_iter_const, state_iter);
void Herm_comm1(state_iter_const,const state_iter_const ,const state_iter_const,const state_iter_const, state_iter);
state_type outer_prod(const state_type&,const state_type&,int,int);
state_type add_expanded_basis(const state_type&,int,int, int,int,state_type&);
state_type mult_m(const state_type&,const state_type&,int,int);
state_type conj_transpose(const state_type&,int);
void add_m(state_iter,state_iter, state_iter_const);
void const_mult_m(state_type&, std::complex<double>);


double fidelity(state_iter_const,state_iter_const,int);
double Hermitian_fidelity(state_type&,state_type&,int);
double trace(const state_type&);
#endif
