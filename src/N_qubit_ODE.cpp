#include "N_qubit_ODE.h"
#include "Helper_functions.h"
#include <cmath>
#include "mex.h"

static const std::complex<double> i(0.0, 1.0);
static const int sigma_z[4] = {1,0,0,-1};
static const std::complex<double> sigma_minus[4] = {0,0,1,0};



N_qubit_ODE::N_qubit_ODE(std::vector<RE_qubit> _qubits, std::vector<std::complex<double> > _int_matrix,
double* _pulse_sequence,double* _pulse_time,double* _laser_frequency,int _pulse_length,int _num_pulses)
    : qubits(_qubits), int_matrix(_int_matrix), pulse_sequence(_pulse_sequence), pulse_time(_pulse_time)
        ,laser_frequencies(_laser_frequency),pulse_length(_pulse_length), prev_time(0), num_pulses(_num_pulses)
{

    this->num_qubits =qubits.size();
}

int N_qubit_ODE::get_num_states(int qubit_num)
{
    return qubits[qubit_num].get_num_states();
}

double N_qubit_ODE::get_pulse_value(int kk, double t)
{

    double* time_loc = std::lower_bound(this->pulse_time, this->pulse_time + pulse_length, t);

    double curr_field=0;
    if(*time_loc == t) // we know values for this time
    {
            int idx = time_loc - pulse_time;
            curr_field= pulse_sequence[idx*num_pulses+kk];
    }
     else // interpolate
    {
        double* nearest_time = find_nearest(pulse_time, pulse_time + pulse_length, t);
        if(nearest_time == pulse_time + pulse_length - 1) // we're past the last element
        {
            curr_field=(pulse_sequence[(pulse_length-1)*num_pulses + num_pulses - 1]);
        } else if(nearest_time == pulse_time) // we're before the first element
        {
            curr_field=(pulse_sequence[kk]);
        } else {
            int idx = nearest_time - pulse_time;
            curr_field=interpolate(*nearest_time, *(nearest_time + 1), pulse_sequence[idx*num_pulses+kk], pulse_sequence[(idx+1)*num_pulses+kk], t);
        }
    }

return curr_field;
}



void N_qubit_ODE::operator()(const state_type& curr_state, state_type& drhodt, const double t)
{

    std::vector<double> curr_field(num_pulses);
    std::vector<double> curr_freqs(num_pulses);

    int num_removed =0;
    for (int ii=0;ii!=num_pulses;++ii)
    {
        curr_field[ii-num_removed]=get_pulse_value(ii,t);
        curr_freqs[ii-num_removed]=this->laser_frequencies[ii];
        if (curr_field[ii-num_removed]==0.0) //then don't simulate it
        {
            curr_field.erase(curr_field.begin()+ii-num_removed);
            curr_freqs.erase(curr_freqs.begin()+ii-num_removed);
            ++num_removed;

        }

    }

    int num_states = get_num_states(0);

    state_type H_full(std::pow(num_states,2*this->num_qubits),std::complex<double>(0,0));

    // evolve each of our qubits independently
    for(int ii = 0; ii != num_qubits; ++ii) {
        state_type H_sq(std::pow(num_states,2*this->num_qubits),std::complex<double>(0,0));
        double dt = pulse_time[1]-pulse_time[0];
        // find the single qubit Hamiltonian
        qubits[ii].Hamiltonian(t,dt,curr_freqs,curr_field,H_sq.begin());

        add_expanded_basis(H_sq,num_states,num_states,ii,this->num_qubits-ii-1,H_full);
    }


    //add the interactions
    add_m(H_full.begin(),H_full.end(),int_matrix.begin());

    for (int ii=0;ii!=drhodt.size();++ii)
    {
        drhodt[ii]=0.0;
    }
    // now commute our full Hamiltonian with the density matrix
    Herm_comm(curr_state.begin(), curr_state.end(), H_full.begin(), H_full.end(), drhodt.begin());


    //add the constant factor
    const_mult_m(drhodt,i);

    add_Lindblad(drhodt,curr_state);

}

//add Lindblad terms to drhodt
void N_qubit_ODE::add_Lindblad(state_type& drhodt, const state_type& rho)
{
    int m_dim=0;
    int num_states1=get_num_states(0);
    int num_states2=1;
    if (this->num_qubits ==1)
    {
        m_dim = get_num_states(0);
    }
    else
    {
        m_dim = get_num_states(0)*get_num_states(1);
        num_states2=get_num_states(1);
    }

    for (int row=0; row != m_dim; ++row)
    {
            for (int col=row; col!= m_dim;++col)
            {
                    //work out where we are
                    int qubitA_row = row/num_states2; //this line relies on int division rounding to floor
                    int qubitA_col = col/num_states2;

                    int qubitB_row = row % num_states2;
                    int qubitB_col = col % num_states2;

                    //compute relaxation
                    if (qubitA_row == qubitA_col) //A relaxation
                    {
                        
                        for (int relax_idx=0; relax_idx != qubitA_row;++relax_idx) //contributions due to relaxation from this state
                        {
                            drhodt[row*m_dim+col]-= (1/qubits[0].T1[qubitA_row*num_states1+relax_idx])*rho[row*m_dim+col]*qubits[0].decay_prob[qubitA_row*num_states1+relax_idx];

                        }
                        for (int relax_idx=qubitA_row+1; relax_idx != num_states1;++relax_idx) //contributions due to relaxation to this state
                        {
                            int relax_state = (relax_idx*num_states2+qubitB_row)*m_dim+(relax_idx*num_states2+qubitB_col);
                            drhodt[row*m_dim+col]+= (1/qubits[0].T1[qubitA_row*num_states1+relax_idx])*rho[relax_state]*qubits[0].decay_prob[qubitA_row*num_states1+relax_idx];
                        }
                    }
                    else //compute qubitA dephasing
                    {
                       drhodt[row*m_dim+col] -=   qubits[0].transition_lw[qubitA_row*num_states1+qubitA_col]*rho[row*m_dim+col];
                    }

                    if (this->num_qubits >1)
                    {

                            if (qubitB_row == qubitB_col)//B relaxation
                            {
                                for (int relax_idx=0; relax_idx != qubitB_row;++relax_idx) //contributions due to relaxation from this state
                                {
                                    drhodt[row*m_dim+col]-= 1/qubits[1].T1[qubitB_row*num_states2+relax_idx]*rho[row*m_dim+col]*qubits[1].decay_prob[qubitB_row*num_states2+relax_idx];
                                }
                                for (int relax_idx=qubitB_row+1; relax_idx != num_states2;++relax_idx) //contributions due to relaxation to this state
                                {
                                    int relax_state = (qubitA_row*num_states2+relax_idx)*m_dim+(qubitB_col*num_states2+relax_idx);
                                    drhodt[row*m_dim+col]+= 1/qubits[1].T1[qubitB_row*num_states2+relax_idx]*rho[relax_state]*qubits[1].decay_prob[qubitB_row*num_states2+relax_idx];
                                }
                            }
                            else //compute qubitB dephasing
                            {
                              drhodt[row*m_dim+col] -=  qubits[1].transition_lw[qubitB_row*num_states2+qubitB_col]*rho[row*m_dim+col];

                            }
                    }



                    drhodt[col*m_dim+row]=std::conj(drhodt[row*m_dim+col]);
                    }

    }
}

