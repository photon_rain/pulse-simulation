#include <complex>
#include <vector>
#include "RE_qubit.h"

typedef std::vector<std::complex<double> > state_type;

class N_qubit_ODE
{
public:
    void operator()(const state_type& p, state_type& dpdt, const double);
    N_qubit_ODE(std::vector<RE_qubit>, std::vector<std::complex<double> >,double*,double*,double*,int,int);
    state_type Optical_Hamiltonian(int,double);
    N_qubit_ODE() {};
    int num_qubits;
    int num_pulses;
    int get_num_states(int);
    double get_pulse_value(int, double);
private:
    void add_Lindblad(state_type&, const state_type&);
    std::vector<RE_qubit> qubits;
    std::vector<state_type> Lindblad_matrices;
    state_type int_matrix;

        double prev_time;
        //laser properties
        double* pulse_sequence;
    double* pulse_time;
    double* laser_frequencies;
    int pulse_length;
};
