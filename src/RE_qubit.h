#ifndef RE_qubit_H
#define RE_qubit_H

#include <complex>
#include <vector>

typedef std::vector<std::complex<double> > state_type;
typedef std::vector<std::complex<double> >::iterator state_iter;
typedef std::vector<std::complex<double> >::const_iterator state_iter_const;

class RE_qubit
{
public:
    RE_qubit(double*,double*,double*,double*,double*,int);
    RE_qubit() {};
    void Hamiltonian(const double,const double,std::vector<double>,std::vector<double>,state_iter);
    void Optical_Hamiltonian(const double,const double,double,int,state_iter);
    int get_num_states();

    double* energy_levels;
    double* basis_levels;
    double* transition_lw;
    std::vector<double> decay_prob;
    double* T1;
    double* osc_strengths;
    state_type H; // means we don't have to allocate memory each time
    int num_states;


private:
    //allows us to reduce the number of matrix elements
    std::vector<std::vector<std::pair<int,int> > > res_trans;
    std::vector<double> res_freqs; //frequency these transitions were calculated at

    void find_resonant(double,double,int);
};

#endif
