# Pulse Simulation

For simulating the effect of arbitrary pulses on a rare-earth ion in the presence of decoherence.